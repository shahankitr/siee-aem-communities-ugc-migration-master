package com.scea.psn.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.PropertyType;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.codec.binary.Base64;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

@Service(value = Servlet.class)
@Component(immediate = true, metatype = true)
@Properties({ @Property(name = "sling.servlet.paths", value = "/services/migration/user"),
		@Property(name = "service.description", value = "CreateUser"),
		@Property(name = "label", value = "CreateUser") })
public class UserImportServlet extends SlingAllMethodsServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Reference
	ResourceResolverFactory resolverFactory;

	private static Logger logger = LoggerFactory.getLogger(UserImportServlet.class);
	
	public static String message=new String();

	protected final void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {

		final ResourceResolver resolver = request.getResourceResolver();

		final RequestParameter[] fileRequestParameters = request.getRequestParameters("file");
		// final String path = request.getRequestParameter("path").getString();
		List<com.scea.psn.bean.User> users = new ArrayList<com.scea.psn.bean.User>();

		Calendar cal=Calendar.getInstance();
		cal.set(2017, 07, 22);

		Long sourceId=cal.getTimeInMillis();
		
		ResourceResolver adminResolver = null;
		Session adminSession = null;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(ResourceResolverFactory.SUBSERVICE, "user-migration");
		try {
			adminResolver = resolverFactory.getServiceResourceResolver(param);
			adminSession = adminResolver.adaptTo(Session.class);
			final UserManager userManager = adminResolver.adaptTo(UserManager.class);

			if (null == userManager.getAuthorizable("community-members")) {

				Group group = userManager.createGroup("community-members", new SimplePrincipal("community-members"),
						"/home/groups");

				Value value = adminSession.getValueFactory().createValue("psn_community_user", PropertyType.STRING);
				group.setProperty("./profile/givenName", value);

				value = adminSession.getValueFactory().createValue("Community User Group", PropertyType.STRING);
				group.setProperty("./profile/aboutMe", value);

				value = adminSession.getValueFactory().createValue("psncommunity788@gmail.com", PropertyType.STRING);
				group.setProperty("./profile/email", value);
				adminSession.save();
				adminResolver.commit();
				response.getWriter().write("Group Created Successfully..");

			} else {
				response.getWriter().write("Group already exist..");
			}
			if (fileRequestParameters != null && fileRequestParameters.length > 0
					&& !fileRequestParameters[0].isFormField()) {
				if (fileRequestParameters[0].getFileName().endsWith(".json")) {

					final InputStream inputStream = fileRequestParameters[0].getInputStream();
					final JsonParser jsonParser = new JsonFactory().createParser(inputStream);
					try {
						users.addAll(getUserList(jsonParser));
						response.getWriter().write(saveUsers(users, userManager, adminResolver, adminSession, response, sourceId));
					} catch (Exception e) {
						response.getWriter().write("User Status:"+message);
						response.getWriter().write(printTraceStack(e));
					}

				} else if (fileRequestParameters[0].getFileName().endsWith(".zip")) {
					ZipInputStream zipInputStream;
					try {
						zipInputStream = new ZipInputStream(fileRequestParameters[0].getInputStream());
					} catch (IOException e) {
						throw new ServletException("Could not open zip archive");
					}
					try {
						int counter = 0;
						ZipEntry zipEntry = zipInputStream.getNextEntry();
						while (zipEntry != null) {
							 users = new ArrayList<com.scea.psn.bean.User>();
							final JsonParser jsonParser = new JsonFactory().createParser(zipInputStream);
							try {
								users.addAll(getUserList(jsonParser));
								response.getWriter().write(saveUsers(users, userManager, adminResolver, adminSession, response, sourceId));
							} catch (Exception e) {
								response.getWriter().write("User Status:"+message);
								response.getWriter().write(printTraceStack(e));
							}
							zipInputStream.closeEntry();
							zipEntry = zipInputStream.getNextEntry();
						}
					} finally {
						zipInputStream.close();
					}
				} else {
					throw new ServletException("Unrecognized file input type");
				}
			} else {
				throw new ServletException("No file provided for UGC data");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.getWriter().write(printTraceStack(e));
		} finally {
			if (adminResolver != null)
				adminResolver.close();
		}

	}

	public List<com.scea.psn.bean.User> getUserList(JsonParser jsonParser) throws Exception {
		JsonToken jsonToken = jsonParser.nextToken(); // get the first token
		List<com.scea.psn.bean.User> users = new ArrayList<com.scea.psn.bean.User>();
		if (jsonToken.equals(JsonToken.START_OBJECT)) {
			jsonParser.nextToken();
			jsonParser.nextToken();
			com.scea.psn.bean.User user = null;
			if (jsonParser.getCurrentToken().equals(JsonToken.START_ARRAY)) {
				jsonParser.nextToken();
				jsonParser.nextToken();
				user = new com.scea.psn.bean.User();
				while (jsonParser.getCurrentToken().equals(JsonToken.FIELD_NAME)) {

					String label = jsonParser.getCurrentName();
					jsonParser.nextToken();
					String value = jsonParser.getValueAsString();
					if (label.equals("email")) {
						user.setEmail(value);
					} else if (label.equals("handle")) {
						user.setHandle(value);

					} else if (label.equals("password")) {
						user.setPassword(value);
					} else if (label.equals("avataarUrl")) {
						user.setAvataarImg(value);
					} else if (label.equals("registrationTime")) {
						user.setRegistrationTime(Long.valueOf(value));
					} else if (label.equals("lastVisitTime")) {
						user.setLastVisitTime(Long.valueOf(value));
					}
					else if (label.equals("mimeType")) {
						user.setProfileMimeType(value);
					}

					jsonParser.nextToken();
					if (jsonParser.getCurrentToken().equals(JsonToken.END_OBJECT)) {
						users.add(user);
						jsonParser.nextToken();
					}
					if (jsonParser.getCurrentToken().equals(JsonToken.START_OBJECT)) {
						user = new com.scea.psn.bean.User();
						jsonParser.nextToken();
					}
				}
			}
		}
		return users;
	}

	public String printTraceStack(Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String stackTrace = sw.toString();
		logger.error(stackTrace);
		return stackTrace;

	}

	protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	private static class SimplePrincipal implements Principal {
		protected final String name;

		public SimplePrincipal(String name) {
			if ("".equals(name)) {
				throw new IllegalArgumentException("Principal name cannot be blank.");
			}
			this.name = name;
		}

		public String getName() {
			return name;
		}

		@Override
		public int hashCode() {
			return name.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Principal) {
				return name.equals(((Principal) obj).getName());
			}
			return false;
		}
	}
	
	public String saveUsers(List<com.scea.psn.bean.User> users, UserManager userManager, ResourceResolver adminResolver, Session adminSession, SlingHttpServletResponse response, Long sourceId) throws Exception{
		String dirLoc="";
		String userBeforeAdminSession="";
		String userAfterAdminSession="";
		String alreadySavedUser="";
		int count=0;
		for (com.scea.psn.bean.User user : users) {
			if (userManager.getAuthorizable(user.getHandle()) == null) {
				 dirLoc="";
				for(int i=0;i<user.getHandle().length();i++){
					dirLoc=dirLoc+user.getHandle().charAt(i)+"/";
					if(i==2){
						break;
					}
				}
				User userDB = userManager.createUser(user.getHandle(), user.getPassword(),
						new SimplePrincipal(user.getHandle()), "/home/users/community/"+dirLoc);
				userBeforeAdminSession="User Before Saving:"+userDB.getID();
				Value value = adminSession.getValueFactory().createValue(user.getHandle(), PropertyType.STRING);
				userDB.setProperty("./profile/familyName", value);

				value = adminSession.getValueFactory().createValue(user.getEmail().toLowerCase(), PropertyType.STRING);
				userDB.setProperty("./profile/email", value);
				
				value = adminSession.getValueFactory().createValue("", PropertyType.STRING);
				userDB.setProperty("./profile/aboutMe", value);
				
				value = adminSession.getValueFactory().createValue(user.getAvataarImg(), PropertyType.STRING);
				userDB.setProperty("./profile/avatarURL", value);
				
				value = adminSession.getValueFactory().createValue("", PropertyType.STRING);
				userDB.setProperty("./profile/city", value);
				value = adminSession.getValueFactory().createValue("", PropertyType.STRING);
				userDB.setProperty("./profile/displayName", value);
				value = adminSession.getValueFactory().createValue("", PropertyType.STRING);
				userDB.setProperty("./profile/gender", value);
				value = adminSession.getValueFactory().createValue("", PropertyType.STRING);
				userDB.setProperty("./profile/givenName", value);
				value = adminSession.getValueFactory().createValue("", PropertyType.STRING);
				userDB.setProperty("./profile/jobTitle", value);
				value = adminSession.getValueFactory().createValue("en", PropertyType.STRING);
				userDB.setProperty("./profile/language", value);
				value = adminSession.getValueFactory().createValue("", PropertyType.STRING);
				userDB.setProperty("./profile/region", value);
				value = adminSession.getValueFactory().createValue("", PropertyType.STRING);
				userDB.setProperty("./profile/streetAddress", value);
				value = adminSession.getValueFactory().createValue("", PropertyType.STRING);
				userDB.setProperty("./profile/url", value);
				
				value = adminSession.getValueFactory().createValue(sourceId.toString(), PropertyType.LONG);
				userDB.setProperty("./profile/sourceId", value);

				Group group = (Group) (userManager.getAuthorizable("community-members"));
				group.addMember(userManager.getAuthorizable(userDB.getID()));

				
			//	response.getWriter().write(userDB.getID()+", ");
				adminSession.save();
				adminResolver.commit();
				userAfterAdminSession="Recent Saved User:"+userDB.getID();
				message=alreadySavedUser+"\t"+userBeforeAdminSession+"\t"+userAfterAdminSession;
				/*if (user.getAvataarImg() != null || !"".equals(user.getAvataarImg())) {
					try {
						InputStream is=new ByteArrayInputStream(Base64.decodeBase64(user.getAvataarImg()));
						Binary binary =
								  adminSession.getValueFactory().createBinary(is);
						Node photosNode = adminSession.getNode(userDB.getPath() + "/profile").addNode("photos",
								"sling:Folder");
						Node imagePrimaryNode = photosNode.addNode("primary", "sling:Folder");

						Node myNewNode = imagePrimaryNode.addNode("image", "nt:file");
						Node contentNode = myNewNode.addNode("jcr:content", "nt:resource");

						contentNode.setProperty("jcr:data", binary);
						contentNode.setProperty("jcr:lastModified", Calendar.getInstance());
						contentNode.setProperty("jcr:mimeType", user.getProfileMimeType());
					} catch (Exception e) {
						response.getWriter().write("Exception: " + e.getMessage());
					}
				}*/
				

			} else {
				alreadySavedUser="Already Exists:"+user.getHandle();
			}
			
			
		}
		return alreadySavedUser+"\t"+userBeforeAdminSession+"\t"+userAfterAdminSession;
	}

}
