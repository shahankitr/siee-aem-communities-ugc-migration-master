package com.scea.psn.bean;

public class User {

	private String email;
	private String password;
	private String handle;
	private String avataarImg;
	private Long registrationTime;
	private Long lastVisitTime;
	private String profileMimeType;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getHandle() {
		return handle;
	}
	public void setHandle(String handle) {
		this.handle = handle;
	}
	public String getAvataarImg() {
		return avataarImg;
	}
	public void setAvataarImg(String avataarImg) {
		this.avataarImg = avataarImg;
	}
	public Long getRegistrationTime() {
		return registrationTime;
	}
	public void setRegistrationTime(Long registrationTime) {
		this.registrationTime = registrationTime;
	}
	public Long getLastVisitTime() {
		return lastVisitTime;
	}
	public void setLastVisitTime(Long lastVisitTime) {
		this.lastVisitTime = lastVisitTime;
	}
	public String getProfileMimeType() {
		return profileMimeType;
	}
	public void setProfileMimeType(String profileMimeType) {
		this.profileMimeType = profileMimeType;
	}
	
	
}

